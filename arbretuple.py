#Pour arbre de tuples

def est_dans(val, arbre, n)->bool:
    for val in arbre:
        if racine is n:
            return True
        elif gauche or droite is n:
            return True
        else:
            return False


#from graphviz import Digraph

class Noeud:
    """
    Un noeud a une valeur et pointe vers deux autres noeuds (petit et
    grand) ou éventuellement le vide.
    On insère de nouvelles valeurs en partant de la racine du noeud et
    en bifurquant selon la comparaison avec la valeur du noeud.

    """
#deja fait
    def __init__(self, val):
        """
        Un noeud a toujours une valeur mais pointe vers un autre noeud
        ou éventuellement le vide (None)
        """
        self.__val = val
        self.__grand = None
        self.__petit = None

#fait
    def insere(self, val):
        if val < self.__val:
        if self.__grand is None:
            self.__grand = Node(val)
            self.__grand.parent = self
        else:
            self.__grand.insert(val)
        elif val > self.__val:
            if self.__petit is None:
                self.__petit = Node(val)
                self.__petit.parent = self
            else:
                self.__petit.insert(val)

  #fait          
    def hauteur(self) -> int:
        return 1 + max(
            self.left.get_height() 
            if self.left 
            else -1, 
            self.right.get_height()
            if self.right 
            else -1)
    
#fait 
    def nb_noeuds(self) -> int:
        n = 0
        while self.__petit and self.__grand is not None:
            n = n+1
            return n

#fait    
    def est_feuille(self) -> bool:
        if self.__petit or self.__grand - 1 is not None:
            return True
        else:
            return False

#fait
    def nb_feuilles(self) -> int:
        """
        J'utilise bool ici car les booléens en python sont un sous type des entiers
        """
        return bool(self.__petit) + bool(self.__grand)
     

#fait
    def visite_pre(self) -> None:
        """
        Que fait ce code ?
        Expliquez la différence avec les deux suivants
        """
        print(self.__val)
        for cote in [self.__petit, self.__grand]:
            if cote is not None:
                cote.visite_pre()

    #Ce code va nous donner les dernières noeuds de l'arbre
    #La différence avec les autres programmes est que dans le programme 'visite_post', on nous donne la derniere valeur de l'arbre
    #et dans 'visite_inf', on nous donne la derniere valeur de chaques (branche petit et branche grande)
    #(Quand je parle de derniere valeur de l'arbre ou de derniere valeur de la branche cela represente la valeur la plus basse dans l'arbre)

    def visite_post(self) -> None:
        for cote in [self.__petit, self.__grand]:
            if cote is not None:
                cote.visite_post()
        print(self.__val)

    def visite_inf(self) -> None:
        if self.__petit is not None:
            self.__petit.visite_inf()
        print(self.__val)
        if self.__grand is not None:
            self.__grand.visite_inf()

from collection import deque
#Je n'ai pas reussi a faire ce programme avec la classe noeud donc je l'ai fait avec les listes python

def visite_niveau(arbre, debut):
    visiter = []
    queue = deque()
    queue.append(debut)
    while queue:
        noeud = queue.poppetit()
        if noeud not in visiter:
            visiter.append(noeud)
            non_visiter = [n for n in arbre[noeud] if n not in visiter]
            queue.extend(non_visiter)
    return visiter

#fait
    def mini(self):
        noeud = self
        while noeud.__grand:
            noeud = noeud.__grand
        return noeud
        
#fait
    def maxi(self):
        noeud = self
        while noeud.__petit:
            noeud = noeud.__petit
        return noeud


#Ajout + :

#Verification d'equilibre de l'arbre
def verif_equilibre(self):
    petit = self.__petit._check_balance() if self.__petit else -1
    grand = self.__grand._check_balance() if self.grand else -1
    if abs(left - right) > 1:
        raise ValueError('Unbalanced tree.')
    return max(left, right) + 1

def is_balanced(self):
    try:
        self.verif_equilibre()
        return True
    except ValueError:
        return False
        
    #  Outils de représentation
    
    def viz(self):
        """Renvoie un objet graphviz pour la visualisation graphique de l'arbre"""
        def representation(dot, noeud, aretes):
            if noeud is not None:
                dot.node(str(id(noeud)), str(noeud.valeur))
                # Appel récursif de la fonction representation
                if noeud.__petit is not None:
                    representation(dot, noeud.__petit, aretes)
                    aretes.append((str(id(noeud)) , str(id(noeud.__petit))))
                if noeud.__grand is not None:
                    representation(dot, noeud.__grand, aretes)
                    aretes.append((str(id(noeud)) , str(id(noeud.__grand))))
                    
        dot = Digraph(comment="Arbre binaire", format='svg')
        aretes = []
        representation(dot, self.racine, aretes)
        dot.edges(aretes)
        return dot

    def affiche(self):
        """
        """
        s = self.viz()
        s.graph_attr['ordering']='out'
        return s


#Exercice: Arbres binaires de recherche

    def est_arbre(self)->bool:
        if Arbre is []:
            return False
        elif self.__petit < self.__grand:
            return True
        else:
            return False 

    def 


#Exercice : Codage de Huffman

    def 
        